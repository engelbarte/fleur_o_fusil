public class Solution {
    private int [] regles;
    private int fitness;

    public Solution() {
        this.regles = new int [6 * 6 * 6];
        this.fitness = 0;
    }

    public Solution(Solution s) {
        this.regles = s.getRegles().clone();
        this.fitness = s.getFitness();
    }

    public Solution(int [] regles) {
        this.regles = regles.clone();
        this.fitness = 0;
    }

    public void affRules() {
        for(int i = 0; i < 216; i++) {
            System.out.print(this.regles[i] + " ");
        }
        System.out.println();
    }

    public void updateOneRule(int where, int what) {
        this.regles[where] = what;
    }

    public int getOneRule(int where) {
        return this.regles[where];
    }

    public int [] getRegles() {
        return regles;
    }

    public int getFitness() {
        return fitness;
    }

    public void setFitness(int fitness) {
        this.fitness = fitness;
    }

    public String toString() {
        return "Fitness : " + this.getFitness();
    }
}