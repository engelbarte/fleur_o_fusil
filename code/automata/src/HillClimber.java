import java.util.Random;
import java.io.*;

public class HillClimber {
    Solution solution;
    Solution neighbor;

    public HillClimber() {
        this.solution = new Solution();
        this.neighbor = new Solution();
    }

    public Solution getNeighbor() {
        return neighbor;
    }

    public Solution getSolution() {
        return solution;
    }

    public void setSolution(Solution solution) {
        this.solution = new Solution(solution);
    }

    public void setNeighbor(Solution neighbor) {
        this.neighbor = new Solution(neighbor);
    }

    public boolean firstImprovementThree(int [] index, Automata automate) {
        int tmp_value, tmp_fitness, test;
        for (int i = 0; i < index.length; i++) {
            tmp_value = this.getSolution().getOneRule(index[i]);
            tmp_fitness = this.getSolution().getFitness();
            for (int j = 0; j < 4; j++) {
                if (j != tmp_value) {
                    this.getSolution().updateOneRule(index[i], j);
                    if ((test = automate.f(this.getSolution().getRegles(), 20)) > tmp_fitness) {
                        this.getSolution().setFitness(test);
                        System.out.println(this.getSolution());
                        return false;
                    }
                }
            }
            this.getSolution().updateOneRule(index[i], tmp_value);
        }
        return true;
    }

    public boolean firstImprovementTwo(int [] index, Automata automate, Initialization init) {
        int tmp_value, tmp_fitness, test;
        Random gen = new Random();
        for (int i = 0; i < index.length; i++) {
            tmp_value = this.getNeighbor().getOneRule(index[i]);
            tmp_fitness = this.getNeighbor().getFitness();
            this.getNeighbor().updateOneRule(index[gen.nextInt(index.length)], gen.nextInt(4));
            if ((test = automate.f(this.getNeighbor().getRegles(), 20)) >= tmp_fitness) {
                this.getNeighbor().setFitness(test);
                return false;
            }
            this.getNeighbor().updateOneRule(index[i], tmp_value);
        }
        return true;
    }

    public boolean firstImprovement(int [] index, Automata automate) {
        int tmp_value, tmp_fitness, test;
        for (int i = 0; i < index.length; i++) {
            tmp_value = this.getNeighbor().getOneRule(index[i]);
            tmp_fitness = this.getNeighbor().getFitness();
            for (int j = 0; j < 4; j++) {
                if (j != tmp_value) {
                    this.getNeighbor().updateOneRule(index[i], j);
                    if ((test = automate.f(this.getNeighbor().getRegles(), 20)) > tmp_fitness) {
                        this.getNeighbor().setFitness(test);
                        return false;
                    }
                }
            }
            this.getNeighbor().updateOneRule(index[i], tmp_value);
        }
        return true;
    }

    public void localSearch(int [] index, Automata automate, Initialization init) {
        boolean optimumLocal = false;

        while (!optimumLocal) {
            optimumLocal = this.firstImprovement(index, automate);
            //optimumLocal = this.firstImprovementThree(index, automate);
            //optimumLocal = this.firstImprovementTwo(index, automate, init);
            // commenté si 1st improvment 3

            if (this.getNeighbor().getFitness() > this.getSolution().getFitness()) {
                this.setSolution(this.getNeighbor());
          //      System.out.println(this.getSolution());
            }

        }
    }

    public static void main(String[] args) {
        boolean optimumLocal = false;
        int[] rules_index = new int[88];
        HillClimber grimpeur = new HillClimber();
        Automata automate = new Automata(20);
        Initialization init = new Initialization();
        Solution tmp = new Solution();

        init.initIndexArray(rules_index, 4);
        init.initRandomly(grimpeur.getSolution().getRegles(), rules_index);
        grimpeur.getSolution().setFitness(automate.f(grimpeur.getSolution().getRegles(), 20));
        grimpeur.setNeighbor(grimpeur.getSolution());

        for (int i = 0; i < 100000000; i++) {
            grimpeur.localSearch(rules_index, automate, init);
            init.muteRandomlyN(grimpeur.getNeighbor().getRegles(), rules_index, 20 - grimpeur.getSolution().getFitness());
        }
        //System.out.println(grimpeur.getSolution());
        //grimpeur.getSolution().affRules();

        // ECRITURE DANS FICHIER

        PrintWriter ecrivain;
        try {
            ecrivain = new PrintWriter(new BufferedWriter(new FileWriter("../" + args[0] + ".out")));

            ecrivain.println(grimpeur.getSolution().getFitness());
            for(int i = 0; i < 216; i++) {
                ecrivain.print(grimpeur.getSolution().getOneRule(i) + " ");
            }
            ecrivain.println();

            ecrivain.close();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
    }
}