/**
 *
 */

import java.util.Random;
import java.util.ArrayList;

/**
 * @author verel
 *
 */
public class Initialization {

	int nbRules = 216;

	Random generator;

	public Initialization() {
		generator = new Random();
	}

	public void init(int [] rules) {
		for(int i = 0; i < nbRules; i++)
			rules[i] = generator.nextInt(4);

		// les regles repos (obligatoires)
		setRule(rules, 0, 0, 0, 0);
		setRule(rules, 5, 0, 0, 0);
		setRule(rules, 0, 0, 5, 0);

		// les regles feu (trés conseillés)
		setRule(rules, 1, 1, 1, 4);
		setRule(rules, 5, 1, 1, 4);
		setRule(rules, 1, 1, 5, 4);

		// les regles a priori (signal de période 2 vers la droite)
		setRule(rules, 1, 0, 0, 2);
		setRule(rules, 2, 0, 0, 1);

		// a priori bord droit
		setRule(rules, 1, 0, 5, 1); // pour taille 2
		setRule(rules, 2, 0, 5, 2);

		// a priori bord gauche (pour la taille 2)
		setRule(rules, 5, 1, 0, 1);
	}

	public void initIndexArray(int [] toReturn, int limit) {
		int i = 0;
		for (int g = 0; g < limit; g++) {
			for (int c = 0; c < limit; c++) {
				for (int d = 0; d < limit; d++) {
					if (!((g == 0 && c == 0 && d == 0) || (g == 1 && c == 1 && d == 1))) {
						toReturn[i] = 36 * g + 6 * c + d;
						i++;
					}
				}
			}
		}
		int b = 5;
		for (int c = 0; c < limit; c++) {
			for (int d = 0; d < limit; d++) {
				if (!((c == 0 && d == 0) || (c == 1 && d == 1) || (c == 1 && d == 0))) {
					toReturn[i] = 36 * b + 6 * c + d;
					i++;
				}
			}
		}
		for (int g = 0; g < limit; g++) {
			for (int c = 0; c < limit; c++) {
				if (!((g == 0 && c == 0) || (g == 1 && c == 1) || (g == 1 && c == 0))) {
					toReturn[i] = 36 * g + 6 * c + b;
					i++;
				}}
		}
	}

	public void initRandomly(int [] rules, int [] index) {
		for(int i = 0; i < index.length; i++)
			rules[index[i]] = generator.nextInt(4);

		// les regles repos (obligatoires)
		setRule(rules, 0, 0, 0, 0);
		setRule(rules, 5, 0, 0, 0);
		setRule(rules, 0, 0, 5, 0);

		// les regles feu (trés conseillés)
		setRule(rules, 1, 1, 1, 4);
		setRule(rules, 5, 1, 1, 4);
		setRule(rules, 1, 1, 5, 4);

		// les regles a priori (signal de période 2 vers la droite)
		setRule(rules, 1, 0, 0, 2);
		setRule(rules, 2, 0, 0, 1);

		// a priori bord droit
		setRule(rules, 1, 0, 5, 1); // pour taille 2
		setRule(rules, 2, 0, 5, 2);

		// a priori bord gauche (pour la taille 2)
		setRule(rules, 5, 1, 0, 1);
	}

	public void findNeighbour(int [] rules, int [] index) {
		int maxSize = generator.nextInt(index.length);
		int toApply = generator.nextInt(4);

		while (toApply == rules[index[maxSize]])
			toApply = generator.nextInt(4);
		rules[index[maxSize]] = toApply;

		// les regles repos (obligatoires)
		setRule(rules, 0, 0, 0, 0);
		setRule(rules, 5, 0, 0, 0);
		setRule(rules, 0, 0, 5, 0);

		// les regles feu (trés conseillés)
		setRule(rules, 1, 1, 1, 4);
		setRule(rules, 5, 1, 1, 4);
		setRule(rules, 1, 1, 5, 4);

		// les regles a priori (signal de période 2 vers la droite)
		setRule(rules, 1, 0, 0, 2);
		setRule(rules, 2, 0, 0, 1);

		// a priori bord droit
		setRule(rules, 1, 0, 5, 1); // pour taille 2
		setRule(rules, 2, 0, 5, 2);

		// a priori bord gauche (pour la taille 2)
		setRule(rules, 5, 1, 0, 1);
	}

	public void muteRandomlyN(int [] rules, int [] index, int n) {
		for (int i = 0; i < n; i++) {
			rules[index[generator.nextInt(88)]] = generator.nextInt(4);
		}

		// les regles repos (obligatoires)
		setRule(rules, 0, 0, 0, 0);
		setRule(rules, 5, 0, 0, 0);
		setRule(rules, 0, 0, 5, 0);

		// les regles feu (trés conseillés)
		setRule(rules, 1, 1, 1, 4);
		setRule(rules, 5, 1, 1, 4);
		setRule(rules, 1, 1, 5, 4);

		// les regles a priori (signal de période 2 vers la droite)
		setRule(rules, 1, 0, 0, 2);
		setRule(rules, 2, 0, 0, 1);

		// a priori bord droit
		setRule(rules, 1, 0, 5, 1); // pour taille 2
		setRule(rules, 2, 0, 5, 2);

		// a priori bord gauche (pour la taille 2)
		setRule(rules, 5, 1, 0, 1);
	}

	/*
	 * Ecrit la regle
	 *
	 * g : etat de la cellule de gauche
	 * c : etat de la cellule centrale
	 * d : etat de la cellule de droite
	 * r : etat de la cellule centale à t+1
	 */
	public void setRule(int [] rules, int g, int c, int d, int r) {
		rules[g * 36 + c * 6 + d] = r;
	}
}
