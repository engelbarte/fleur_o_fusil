import java.io.*;

public class RandomSearch {
    public static void main(String[] args) {
        Automata automate = new Automata(20);
        Initialization init = new Initialization();
        Test save = new Test();
        int [] rules_index = new int [88];
        init.initIndexArray(rules_index, 4);


        int [] rules = new int [6 * 6 * 6];
        int fitness = 0;
        int nFire;

        PrintWriter ecrivain;
        try {
            ecrivain =  new PrintWriter(new BufferedWriter(new FileWriter("the_best_test.out")));

            for (int i = 0; i < 100000; i++) {
                init.initRandomly(rules, rules_index);
                if (automate.f(rules, 20) > fitness) {
                    save.printToFile(fitness, rules, ecrivain);
                    fitness = automate.f(rules, 20);
                }
            }
            ecrivain.close();
        }
        catch (Exception e){
            System.out.println(e.toString());
        }
        System.out.println(fitness);
    }
}