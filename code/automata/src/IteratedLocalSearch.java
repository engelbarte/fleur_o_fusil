import java.util.Random;
import java.io.*;

public class IteratedLocalSearch {
    int nbEval;
    Solution solution;

    public IteratedLocalSearch() {
        this.nbEval = 0;
        this.solution = new Solution();
    }

    public IteratedLocalSearch(Solution s) {
        this.nbEval = 0;
        this.solution = new Solution(s);
    }

    public IteratedLocalSearch(IteratedLocalSearch ils) {
        this.nbEval = ils.getNbEval();
        this.solution = ils.getSolution();
    }

    public Solution getSolution() {
        return solution;
    }

    public int getNbEval() {
        return nbEval;
    }

    public void nbEvalInc() {
        nbEval++;
    }

    public boolean firstImprovement(int [] index, Automata automate) {
        int tmp_value, tmp_fitness, test;
        for (int i = 0; i < index.length; i++) {
            tmp_value = this.getSolution().getOneRule(index[i]);
            for (int j = 0; j < 4; j++) {
                if (j != tmp_value) {
                    this.getSolution().updateOneRule(index[i], j);
                    if ((test = automate.f(this.getSolution().getRegles(), 20)) > this.getSolution().getFitness()) {
                        this.getSolution().setFitness(test);
                        System.out.println(this.getSolution());
                        return false;
                    }
                    this.nbEvalInc();
                }
            }
            this.getSolution().updateOneRule(index[i], tmp_value);
        }
        return true;
    }

    // Recherche locale, hill climber 1st improvment
    public void localSearch(int [] index, Automata automate) {
        // stop quand optimum local
        while (!this.firstImprovement(index, automate));
    }

    public static void main(String[] args) {
        /*
            init de tout
         */
        int[] rules_index = new int[88];
        int maxNbEval = Integer.parseInt(args[0]);
        IteratedLocalSearch grimpeur = new IteratedLocalSearch();
        Automata automate = new Automata(20);
        Initialization init = new Initialization();
        init.initIndexArray(rules_index, 4);

        /*
            fin init
         */

        // Choisir solution initiale x appartenant a X
        init.initRandomly(grimpeur.getSolution().getRegles(), rules_index);
        // Evaluer x avec f
        grimpeur.getSolution().setFitness(automate.f(grimpeur.getSolution().getRegles(), 20));
        // nbEval <- 1
        grimpeur.nbEvalInc();

        /*
            affichage
         */

        System.out.println("Begin");
        System.out.println(grimpeur.getSolution());
        grimpeur.getSolution().affRules();

        /*
            fin affichage
         */

        // boucle Iterated Local Search
        while (grimpeur.nbEval < maxNbEval) {
            grimpeur.localSearch(rules_index, automate);
            init.muteRandomlyN(grimpeur.getSolution().getRegles(), rules_index, 3);
        }

        /*
            affichage
         */

        System.out.println("End : " + grimpeur.getNbEval() + " evaluations");
        System.out.println(grimpeur.getSolution());
        grimpeur.getSolution().affRules();

        /*
            fin affichage
         */
    }
}